import sbt._,Keys._
import scala.tools.nsc.io.Directory

object build{

  val user = "scalaz"
  val branch = "scalaz-seven"

  val zipUrl = "https://github.com/" + user + "/scalaz/archive/" + branch  + ".zip"

  val ignore = Set("")

  val filter = new SimpleFilter(s =>
    ( ! s.contains("project") ) && {
      s.endsWith("scala") || s.endsWith("java")
    }
  )

  val s = sourceGenerators in Compile <+= (sourceDirectory in Compile){
    d =>
    task{
      IO.withTemporaryDirectory{ tmp =>
        println("downloading from " + zipUrl)
        IO.unzipURL(
          url(zipUrl)
          ,tmp
          ,filter
        )
        println("download complete " + zipUrl)
        mvFiles(tmp / ("scalaz-" + branch.replace('/', '-')),d)
      }
    }
  }

  val sxrZip = TaskKey[File]("sxr-zip")

  def now:String = {
    val df = new java.text.SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
    df.format(new java.util.Date)
  }

  def sxr = sxrZip <<= (compile in Compile, crossTarget).map{ (_, dir) =>
    moveToDropbox(dir)
  }

  def moveToDropbox(dir: File): File = {
  //val zipName = "scalaz-" + now + ".zip"
    val zipName = "scalaz.zip"
    val out = dir / zipName
  //IO.zip( deepFiles(dir / "classes.sxr") , out )
    IO.zip( deepFiles( file("..sxr") ) , out )
    IO.move( out , file(sys.props("user.home") + "/Dropbox") / zipName )
    out
  }

  val settings = seq(s,sxr)

  val modules = Seq(
    "core","concurrent","effect","iteratee","xml"//,   "example","typelevel",
    ,"scalacheck-binding","task"
  )

  def deepFiles(base: File):Seq[(File, String)] = {
    val root = new Directory(base)
    root.deepFiles.map{f => f.jfile -> root.jfile.relativize(f.jfile).get.toString}.toSeq
  }

  def mvFiles(from:File,to:File):Seq[File] = {
    def mv(m:String,p:String){
      IO.move(from / m / ("src/" + p + "/scala/scalaz") , to / "scala" / m )
      IO.delete(from / m / "src")
    }
    modules.foreach{ m =>
      mv(m,"main")
    }
    mv("tests","test")
    IO.delete(to / "scala/tests/typelevel")
    to ** "*.scala" get
  }

}
